<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
        parent::__construct();
		$this->load->model("Welcome_model");
	}
	
	public function index() {		
		$this->data['urList'] = $this->Welcome_model->geturls();
		$this->load->view('welcome_message', $this->data);
	}

	public function getChartinfo() {	
		$array = [];	
		$urList = $this->Welcome_model->geturlsChart();
		foreach($urList as $list){
			array_push($array,$list['hits']);
		}
		print_r(json_encode($array));
	}

	public function getChartinfoData() {	
		$array = [];	
		$urList = $this->Welcome_model->getChartinfoData();
		foreach($urList as $list){
			array_push($array,$list['short_code']);
		}
		print_r(json_encode($array));
	}

	public function GetShortUrl(){
		if($_POST){
			$url = urldecode($_POST['url']);
			$slug = $this->createShortUrl($url);
			echo $slug;
		}
	}

	public function createShortUrl($url){
		$urlCheck = $this->Welcome_model->getURLCheck($url);
		if(!empty($urlCheck)){
			return $urlCheck['short_code'];
		}else{
			$short_code = $this->generateUniqueID();
			$result = $this->Welcome_model->insertUrl($short_code,$url);
			if($result){
				return $short_code; 
			}
		}
	}

	public function generateUniqueID(){
		$token = substr(md5(uniqid(rand(), true)),0,6);
		$query = $this->Welcome_model->generateUniqueID($token);
		if ($query > 0) { 
			$this->generateUniqueID(); 
		} else { 
			return $token; 
		}
	}

	public function redirectUrl(){
		if($_POST){
			$slug = urldecode($_POST['redirect']);
			$url = $this->GetRedirectUrl($slug);
			echo $url;
		}
	}

	public function GetRedirectUrl($slug){
		$slug = addslashes($slug);
		$slugCheck = $this->Welcome_model->getslugCheck($slug);
		if (!empty($slugCheck)) {
			$hits = $slugCheck['hits'] + 1;
			$this->Welcome_model->hitsIncrement($hits, $slugCheck['id']);
			return $slugCheck['url'];
		} else {
			return 0;
		}
	}
}
