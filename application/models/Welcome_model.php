<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

	public function geturls() {
        $this->db->select('*');
		$query	=	$this->db->get('url_shorten');
		$this->db->order_by("hits", "desc");
		$resultArr	=	array();
		if($query->num_rows() > 0){
			$resultArr	=	$query->result_array();	
		}
		return $resultArr;
	}

	public function geturlsChart() {
        $this->db->select('hits');
		$query	=	$this->db->get('url_shorten');
		$this->db->order_by("hits", "desc");
		$resultArr	=	array();
		if($query->num_rows() > 0){
			$resultArr	=	$query->result_array();	
		}
		return $resultArr;
	}

	public function getChartinfoData() {
        $this->db->select('short_code');
		$query	=	$this->db->get('url_shorten');
		$this->db->order_by("hits", "desc");
		$resultArr	=	array();
		if($query->num_rows() > 0){
			$resultArr	=	$query->result_array();	
		}
		return $resultArr;
	}
	
    public function getURLCheck($url) {
        $this->db->select('*');
        $this->db->where('url', $url);
		$query	=	$this->db->get('url_shorten');
		$resultArr	=	array();
		if($query->num_rows() > 0){
			$resultArr	=	$query->row_array();	
		}
		return $resultArr;
    }

	public function generateUniqueID($token) {
        $this->db->select('*');
        $this->db->where('short_code', $token);
		$query	=	$this->db->get('url_shorten');
		return $query->num_rows();
	}
	
	public function insertUrl($short_code,$url) {
		$data['url'] = $url;
		$data['short_code'] = $short_code;
		$query = $this->db->insert('url_shorten', $data);
        $res = 0;
		if($query){
			$res = 1;	
		}
		return $res;
	}

	public function getslugCheck($slug) {
        $this->db->select('*');
        $this->db->where('short_code', $slug);
		$query	=	$this->db->get('url_shorten');
		$resultArr	=	array();
		if($query->num_rows() > 0){
			$resultArr	=	$query->row_array();	
		}
		return $resultArr;
    }
	
    public function hitsIncrement($hits, $id) { 
        $res = 0;
		$this->db->where('id', $id);
		$query=$this->db->update('url_shorten',array("hits"=> $hits));
		if($query){
			$res = 1;	
		}
		return $res;
    }
}
