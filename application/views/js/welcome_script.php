<script>
	$(document).ready( function () {
        // var labelData = []
        // var Data = []
        // $.ajax({
        //     url: '<?php echo base_url();?>Welcome/getChartinfo',
        //     type: 'POST',
        //     data: {},
        //     success: function(response)  {
        //             var obj = JSON.parse(response);
        //             labelData = obj;
        //             alert(labelData);
        //     }
        // });

        // $.ajax({
        //     url: '<?php echo base_url();?>Welcome/getChartinfoData',
        //     type: 'POST',
        //     data: {},
        //     success: function(response)  {
        //             var obj = JSON.parse(response);
        //             Data = obj;
        //             alert(Data);
        //     }
        // });
    	$('#myTable').DataTable();
		$("form#url_form").validate({
    	    rules: {
    	        url: { required: true }
    	    },
    	    messages: {
                url: { required: "Please enter a url" }
    	    },
    	    submitHandler: function(form) {
                $.ajax({
                    url: '<?php echo base_url();?>Welcome/GetShortUrl',
                    type: 'POST',
                    data: $("#url_form").serialize(),
                    success: function(response)  {
                        alert('Shorten Url is '+response);
                        $("#redirect").val(response);
                    }
                });
            }
        });

        $("form#redirect_form").validate({
    	    rules: {
    	        redirect: { required: true }
    	    },
    	    messages: {
                redirect: { required: "Please enter a url" }
    	    },
    	    submitHandler: function(form) {
                $.ajax({
                    url: '<?php echo base_url();?>Welcome/redirectUrl',
                    type: 'POST',
                    data: $("#redirect_form").serialize(),
                    success: function(response)  {
                        if(response != 0){
                            window.location.href = response;
                        }else{
                            alert(response);
                        }
                    }
                });
            }
        });

    //     var ctx = document.getElementById('myChart').getContext('2d');
    //     var myChart = new Chart(ctx, {
    //     type: 'bar',
    //     data: {
    //         labels: [Data],
    //         datasets: [{
    //             label: '# of Votes',
    //             data: [labelData],
    //             backgroundColor: [
    //                 'rgba(255, 99, 132, 0.2)',
    //                 'rgba(54, 162, 235, 0.2)',
    //                 'rgba(255, 206, 86, 0.2)',
    //                 'rgba(75, 192, 192, 0.2)',
    //                 'rgba(153, 102, 255, 0.2)',
    //                 'rgba(255, 159, 64, 0.2)'
    //             ],
    //             borderColor: [
    //                 'rgba(255, 99, 132, 1)',
    //                 'rgba(54, 162, 235, 1)',
    //                 'rgba(255, 206, 86, 1)',
    //                 'rgba(75, 192, 192, 1)',
    //                 'rgba(153, 102, 255, 1)',
    //                 'rgba(255, 159, 64, 1)'
    //             ],
    //             borderWidth: 1
    //         }]
    //     },
    //     options: {
    //         scales: {
    //             yAxes: [{
    //                 ticks: {
    //                     beginAtZero: true
    //                 }
    //             }]
    //         }
    //     }
    // });

    });

    function redirectUrl(id){
        $.ajax({
            url: '<?php echo base_url();?>Welcome/redirectUrl',
            type: 'POST',
            data: {redirect:id},
            success: function(response)  {
                if(response != 0){
                    window.location.href = response;
                }else{
                    alert(response);
                }
            }
        });
    }

</script>