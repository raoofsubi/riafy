<?php $this->load->view("includs/header");?>
<?php $this->load->view("js/welcome_script");?>
<body>
<div id="container">
	<div class="row">
		<div class="col-md-6">
			<h4>Put Your Url Here</h4>
			<form id="url_form" method="post">
				<p><input style="width:500px" type="url" name="url" /></p>
				<p><input type="submit" /></p>
			</form>
		</div>
		<div class="col-md-6">
			<h4>Put Your Shorten Url Here</h4>
			<form id="redirect_form" method="post">
				<p><input style="width:500px" type="text" name="redirect" id="redirect" /></p>
				<p><input type="submit" /></p>
			</form>
		</div>
	</div>
	<hr>
	<h4>Analytics</h4>
	<hr>
	<div class="row">
		<div class="col-md-6">
			<div class="white_card">
				<table id="myTable" class="display">
					<thead>
					    <tr>
					        <th>Sl.No</th>
					        <th>Url</th>
					        <th>Shorten Url</th>
					        <th>Hits</th>
					    </tr>
					</thead>
					<tbody>
						<?php $i=1;
						foreach($urList as $row){?>
							<tr>
								<td><?php echo $i; ?></td>
								<td><?php echo $row['url']; ?></td>
								<td><a style="cursor:pointer;" title="Redirect to <?php echo $row['url']; ?>" target="_blank" 
								onclick="redirectUrl('<?php echo $row['short_code']; ?>')"><?php echo $row['short_code']; ?></a></td>
								<td><?php echo $row['hits']; ?></td>
							</tr>
						<?php $i++; } ?>
					</tbody> 
				</table>
			</div>
		</div>
		<div class="col-md-6">
			<canvas id="myChart" width="400" height="400"></canvas>
		</div>
	</div>
</div>
</body>
</html>